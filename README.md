node-arp
======

A module for reading MAC addresses out of the arp table

```js
var arp = require('npm-arp');

arp.getMAC('192.168.0.1', function(err, mac) {
    if (!err) {
        console.log(mac);
    }
});
```
