var util = require('util');

const cidr = require("node-cidr");

var spawn = require('child_process').spawn;
var spawnSync = require('child_process').spawnSync;

const msleep = require('sleep').msleep;


/**
 *
 * LINUX format
	//Parse this format
	//Lookup succeeded : Address                  HWtype  HWaddress           Flags Mask            Iface
	//					IPADDRESS	              ether   MACADDRESS   C                     IFACE
	//Lookup failed : HOST (IPADDRESS) -- no entry
	//There is minimum two lines when lookup is successful
 *
 *
 *  WINDOWS format, multiple NIC-s
	//[blankline]
	//Interface: 192.168.1.54
	//  Internet Address      Physical Address     Type
	//  192.168.1.1           50-67-f0-8c-7a-3f    dynamic
	//[blankline]
	//Interface: 192.168.1.22
	//  Internet Address      Physical Address     Type
	//  192.168.1.1           50-67-f0-8c-7a-3f    dynamic
 */

/**
	//Lookup succeeded : HOST (IPADDRESS) at MACADDRESS on IFACE ifscope [ethernet]
	//Lookup failed : HOST (IPADDRESS) -- no entry
*/

var parseArp = function (msg) {
	var result = [];

	if (process.platform.indexOf('linux') == 0) {

		var rows = msg.trim('\n').split('\n');
		if (rows.length >= 2) {
			rows.splice(0, 1);
			for (var row of rows) {
				var parts = row.split(' ').filter(String);
				var obj = {
					"ip": parts.length == 5 ? parts[0] : parts[1],
					"mac": (parts.length == 5 ? parts[2] : parts[1]).toLowerCase()
				}
				result.push(obj);
			}
		}
	} else if (process.platform.indexOf('win') == 0) {
		var interfaces = msg.split('\r\n\r\n');
		for (var nic of interfaces) {
			var rows = nic.trim("\r\n").split('\r\n');
			rows.splice(0, 2);
			for (var row of rows) {
				var parts = row.split(' ').filter(String);
				var obj = {
					"ip": parts[0],
					"mac": parts[1].replace(/-/g, ':').toLowerCase()
				}
				result.push(obj);
			}
		}
	} else if (process.platform.indexOf('darwin') == 0) {
		var rows = msg.split('\n');
		for (var row of rows) {
			var parts = row.split(' ').filter(String);
			if (parts[3] !== 'no') {
				var obj = {
					"ip": parts[1].replace("(", "").replace(")", ""),
					"mac": parts[3].replace(/^0:/g, '00:').replace(/:0:/g, ':00:').replace(/:0$/g, ':00').replace(/:([^:]{1}):/g, ':0$1:').toLowerCase()
				}
				result.push(obj);
			}
		}
	}
	return result;
}


var getMAC = function (ip, cb) {
	if (process.platform.indexOf('linux') == 0) {
		var ping = spawn("ping", ["-c", "1", ip]);
	} else if (process.platform.indexOf('win') == 0) {
		var ping = spawn("ping", ["-n", "1", ip]);
	} else if (process.platform.indexOf('darwin') == 0) {
		var ping = spawn("ping", ["-c", "1", ip]);
	}

	ping.on('close', function (code) {
		if (process.platform.indexOf('linux') == 0) {
			var arp = spawn("arp", ["-n", ip]);
		} else if (process.platform.indexOf('win') == 0) {
			var arp = spawn("arp", ["-a", ip]);
		} else if (process.platform.indexOf('darwin') == 0) {
			var arp = spawn("arp", ["-n", ip]);
		}

		var buffer = '';
		var errstream = '';
		arp.stdout.on('data', function (data) {
			buffer += data;
		});
		arp.stderr.on('data', function (data) {
			errstream += data;
		});

		arp.on('close', function (code) {
			if (code !== 0) {
				console.log("Error running arp " + code + " " + errstream);
				cb(true, code);
				return;
			}
			var results = parseArp(buffer);
			for (var result of results) {
				if (result.ip == ip) {
					cb(false, result.mac);
					return;
				}
			}
			cb(true, "Count not find IP in arp table: " + ip);
		});
	});
}


var findIPByMacAndCidr = function (mac, in_cidr, cb) {
	console.log("start: getIPByMacAndCidr " + in_cidr);
	mac = mac.replace(/-/g, ':').toLowerCase();
	let min = cidr.cidr.min(in_cidr);
	let max = cidr.ip.toInt(cidr.cidr.max(in_cidr));
	let current = cidr.ip.next(min);
	while(cidr.ip.toInt(current) < max){
		console.log("Pinging: " + current);
		if (process.platform.indexOf('linux') == 0) {
			var ping = spawn("ping", ["-c", "1", current]);
		} else if (process.platform.indexOf('win') == 0) {
			var ping = spawn("ping", ["-n", "1", current]);
		} else if (process.platform.indexOf('darwin') == 0) {
			var ping = spawn("ping", ["-c", "1", current]);
		}

		current = cidr.ip.next(current);

	}
	msleep(1000);
	console.log("end: getIPByMacAndCidr " + in_cidr);
	getIP(mac, cb);
	return;
}


var getIP = function (mac, cb) {
	mac = mac.replace(/-/g, ':').toLowerCase();
	if (process.platform.indexOf('linux') == 0) {
		var arp = spawn("arp", ["-n"]);
	} else if (process.platform.indexOf('win') == 0) {
		var arp = spawn("arp", ["-a"]);
	} else if (process.platform.indexOf('darwin') == 0) {
		var arp = spawn("arp", ["-n"]);
	}

	var buffer = '';
	var errstream = '';
	arp.stdout.on('data', function (data) {
		buffer += data;
	});
	arp.stderr.on('data', function (data) {
		errstream += data;
	});

	arp.on('close', function (code) {
		if (code !== 0) {
			console.log("Error running arp " + code + " " + errstream);
			cb(true, code);
			return;
		}
		var results = parseArp(buffer);
		for (var result of results) {
			if (result.mac == mac) {
				cb(false, result.ip);
				return;
			}
		}
		cb(true, "Count not find mac in arp table: " + mac);
	});
}

module.exports.getMAC = getMAC;
module.exports.getIP = getIP;
module.exports.findIPByMacAndCidr = findIPByMacAndCidr